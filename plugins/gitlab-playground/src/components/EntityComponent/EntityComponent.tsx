import React from 'react';
import { useParams } from 'react-router-dom';
import { useAsync } from 'react-use';
import Alert from '@material-ui/lab/Alert';
import { Grid } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import {
  Progress,
  gitlabAuthApiRef,
  Header,
  Page,
  Content,
  useApi,
  HeaderLabel,
} from '@backstage/core';
import { Icon } from '@iconify/react';
import gitlabIcon from '@iconify-icons/fa-brands/gitlab';
import readmeIcon from '@iconify-icons/fa-brands/readme';
import { graphql } from '@octokit/graphql';
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    margin: 'auto',
    color: theme.palette.text.secondary,
  },
  data: {
    textAlign: 'initial',
    color: 'black',
  },
  dataTitle: {
    display: 'grid',
    padding: '2em',
    textAlign: 'initial',
  },
}));
export const EntityComponent = () => {
  const route2 = useParams();
  const repoName = route2.repoName;
  const classes = useStyles();
  const query = `{
    projects(membership: true search:"${repoName}") {
          nodes {
            name
            description
            createdAt
            webUrl
            forksCount
            projectMembers {
              nodes {
                user {
                  name
                }
              }
            }
            repository {
              tree(ref: "master") {
                lastCommit {
                  authorName
                  authoredDate
                  title
                }
              }
              blobs(paths: "README.md") {
                nodes {
                  rawTextBlob
                }
              }
            }
            mergeRequests{
              nodes{
                author{
                  name
                }
                approvedBy{
                  nodes{
                    name
                    assignedMergeRequests{
                      nodes{
                        approved
                      }
                    }
                  }
                }
                approvalsRequired
              }
            }
          }
        }
      }`;
  type Node = {
    name: string;
    description: string;
    lastActivityAt: string;
    webUrl: string;
    forksCount: string;
    projectMembers: ProjectMembers;
    repository: Repository;
    mergeRequest: MergeRequest;
  };
  type ProjectMembers = {
    nodes: ProjectNode[];
  };
  type ProjectNode = {
    user: User;
  };
  type User = {
    name: string;
  };
  type Repository = {
    tree: Tree;
    blobs: RepositoryBlob;
  };
  type Tree = {
    lastCommit: Lastcommit;
  };
  type RepositoryBlob = {
    nodes: BlobNode[];
  };
  type BlobNode = {
    rawTextBlob: string;
  };
  type Lastcommit = {
    authorName: string;
    authoredDate: string;
    title: string;
  };
  type MergeRequest = {
    node: mergeRequestNode[];
  };
  type mergeRequestNode = {
    author: author;
    approvedBy: approvedBy;
    approvalsRequired: string;
  };
  type author = {
    name: string;
  };
  type approvedBy = {
    nodes: approvedByNode[];
  };
  type approvedByNode = {
    name: string;
    assignedMergeRequest: assignedMergeRequest;
  };
  type assignedMergeRequest = {
    nodes: assignedMergeRequestNode[];
  };
  type assignedMergeRequestNode = {
    approved: string;
  };
  type Viewer = {
    projects: {
      nodes: Node[];
    };
  };
  type GridValueProps = {
    viewer: Viewer;
  };
  const GridValue = ({ viewer }: GridValueProps) => {
    const url = viewer.projects.nodes
      .map(item => {
        return item.webUrl;
      })
      .toString();
    const check = viewer.projects.nodes[0].repository.tree.lastCommit;
    const array = [];
    if (check) {
      array.push(check);
    }
    const readMe = viewer.projects.nodes.map(item => {
      return item.repository.blobs.nodes.map(item1 => {
        return item1.rawTextBlob;
      });
    });
    // const mergeRequest = viewer.projects.nodes.map(item => {
    //   return item.mergeRequest.node.map(item1 => {
    //     return item1.author.name;
    //   });
    // });
    // console.log(mergeRequest);
    return (
      <Page themeId="service">
        <Header
          title={viewer.projects.nodes.map((item, index) => {
            return <div key={index}>{item.name}</div>;
          })}
          subtitle="Repository"
        />
        <Content>
          <div className={classes.root}>
            <Grid container spacing={2}>
              <Grid item>
                <Paper className={classes.paper}>
                  <h1 className={classes.data}>About</h1>
                  <div style={{ display: 'flex' }}>
                    <a
                      href={url}
                      target="_blank"
                      style={{
                        display: 'grid',
                        padding: '2em',
                        justifyItems: 'center',
                      }}
                    >
                      <Icon
                        icon={gitlabIcon}
                        color="#186BBF"
                        width="3em"
                        height="3em"
                      />
                      <span
                        style={{
                          color: '#186BBF',
                          fontWeight: 'bold',
                        }}
                      >
                        View Source
                      </span>
                    </a>
                  </div>
                  <div className={classes.dataTitle}>
                    DESCRIPTION{' '}
                    <span>
                      {Boolean(check) ? (
                        viewer.projects.nodes.map((item, index) => {
                          return (
                            <div key={index} className={classes.data}>
                              {item.description}
                            </div>
                          );
                        })
                      ) : (
                        <div>-</div>
                      )}
                    </span>
                  </div>
                </Paper>
                <Paper
                  className={classes.paper}
                  style={{ display: 'inline-flex' }}
                >
                  <div className={classes.dataTitle}>
                    LAST COMMITTED BY{' '}
                    <span>
                      {Boolean(check) ? (
                        viewer.projects.nodes.map((item, index) => {
                          return (
                            <div key={index} className={classes.data}>
                              {item.repository.tree.lastCommit.authorName}
                            </div>
                          );
                        })
                      ) : (
                        <div>-</div>
                      )}
                    </span>
                  </div>
                  <div className={classes.dataTitle}>
                    <span>
                      COMMIT MESSAGE{' '}
                      {Boolean(check) ? (
                        viewer.projects.nodes.map((item, index) => {
                          return (
                            <div key={index} className={classes.data}>
                              {item.repository.tree.lastCommit.title}
                            </div>
                          );
                        })
                      ) : (
                        <div>-</div>
                      )}
                    </span>
                  </div>
                  <div className={classes.dataTitle}>
                    <span>
                      FORKS COUNT{' '}
                      {viewer.projects.nodes.map((item, index) => {
                        return (
                          <div key={index} className={classes.data}>
                            {item.forksCount}
                          </div>
                        );
                      })}
                    </span>
                  </div>{' '}
                  <div className={classes.dataTitle}>
                    <span>
                      PROJECT MEMBERS
                      {viewer.projects.nodes.map(item => {
                        return item.projectMembers.nodes.map(item1 => {
                          return (
                            <div className={classes.data}>
                              {item1.user.name}
                            </div>
                          );
                        });
                      })}
                    </span>
                  </div>
                </Paper>
              </Grid>
            </Grid>
            <Grid item style={{ paddingTop: '50px' }}>
              <Paper className={classes.paper}>
                <h3
                  style={{
                    color: ' rgba(0, 0, 0, 1)',
                    borderBlock: 'outset',
                    textAlign: 'initial',
                  }}
                >
                  <Icon icon={readmeIcon} style={{ padding: '2px' }} />
                  README.md
                </h3>
                <div style={{ display: 'flex' }}>
                  <a href={url} style={{ display: 'grid', padding: '2em' }}>
                    <span
                      style={{
                        display: 'flex',
                        flexDirection: 'row',
                        color: '#186BBF',
                        fontSize: '20px',
                        textAlign: 'initial',
                      }}
                    >
                      {viewer.projects.nodes.map((item, index) => {
                        return <div key={index}>{item.name}</div>;
                      })}
                    </span>
                  </a>
                </div>
                <div>{readMe}</div>
              </Paper>
            </Grid>
          </div>
        </Content>
      </Page>
    );
  };
  const auth = useApi(gitlabAuthApiRef);
  const { value, loading, error } = useAsync(async (): Promise<any> => {
    const token = await auth.getAccessToken();
    const gqlEndpoint = graphql.defaults({
      baseUrl: 'https://gitlab.com/api',
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
    const viewer = await gqlEndpoint(query);
    return viewer;
  }, []);
  // console.log("value", value);
  if (loading) return <Progress />;
  if (error) return <Alert severity="error">{error.message}</Alert>;
  if (value && value.projects) return <GridValue viewer={value} />;
  return (
    <Page themeId="service">
      <Header title="Repository Name" subtitle="Repository">
        <HeaderLabel label="Owner" value="Team X" />
        <HeaderLabel label="Lifecycle" value="Alpha" />
      </Header>
      <Content>
        <div />
      </Content>
    </Page>
  );
};