import React from 'react';
import { useAsync } from 'react-use';
import Alert from '@material-ui/lab/Alert';
import {
  Table,
  TableColumn,
  Progress,
  gitlabAuthApiRef,
  useApi,
  useRouteRef,
} from '@backstage/core';
import { graphql } from '@octokit/graphql';
// import { entityRouteRef } from '../../routes';
import { Button, Tooltip } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { EntityComponent } from '../EntityComponent';
import { entityRouteRef } from '../../routes';

const slugify = function(text) {
  return text
    .toString()
    .toLowerCase()
    .replace(/\s+/g, '-') // Replace spaces with -
    .replace(/[^\w-]+/g, '') // Remove all non-word chars
    .replace(/--+/g, '-') // Replace multiple - with single -
    .replace(/^-+/, '') // Trim - from start of text
    .replace(/-+$/, '') // Trim - from end of text
}

const query = `{
  projects(membership: true) {
    nodes {
      name
      description
      lastActivityAt
      repository{
        tree(ref: "master"){
          lastCommit{
            authorName
            authoredDate
          }
        }
        blobs(paths: "README.md") {
        nodes{
          rawTextBlob
        }
      }
      }
    }
  }
}`;

type Node = {
  name: string;
  description: string;
  lastActivityAt: string;
  repository: Repository;
}

type Repository = {
  tree: Tree;
  blobs: RepositoryBlob;
}

type Tree = {
  lastCommit: Lastcommit;
}

type RepositoryBlob = {
  nodes: BlobNode[];
}

type BlobNode = {
  rawTextBlob: string;
}

type Lastcommit = {
  authorName: string;
  authoredDate: string;
}

type Viewer = {
  projects: {
    nodes: Node[];
  }
}

type DenseTableProps = {
  viewer: Viewer;
};

export const DenseTable = ({ viewer }: DenseTableProps) => {
  // console.log(viewer)
  // console.log(viewer.projects.nodes[0].repository.blobs.nodes[0].rawTextBlob)
  // console.log(viewer.projects.nodes[0].repository.tree.lastCommit.authorName)
  const myRoute = useRouteRef(entityRouteRef);
  const columns: TableColumn[] = [

    { title: 'Name', field: 'name' },
    { title: 'Description', field: 'description' },
    { title: 'Last Activity', field: 'lastActivityAt' },
    { title: 'Latest Committed By', field: `repository.tree.lastCommit.authorName` },
    { title: 'Latest Committed Date', field: `repository.tree.lastCommit.authoredDate` },
    // { title: 'Read Me', field: `repository.blobs.nodes[0].rawTextBlob` },
    {
      title: 'ACTIONS',
      render: rowData => <Link to={myRoute({repoName: `${slugify(rowData.name)}`})}><Button color="primary"
        variant="contained"
        style={{ textTransform: 'none' }}
        size="small">View</Button></Link>
    }
  ];

  // const row = () => <a href='https://www.google.com'></a>

  return (
    <Table
      title="List Of User's Repositories"
      options={{ search: true, paging: true }}
      columns={columns}
      data={viewer.projects.nodes}
    />
  );
};

export const ExampleFetchComponent = () => {
  const auth = useApi(gitlabAuthApiRef);
  const { value, loading, error } = useAsync(async (): Promise<any> => {
    const token = await auth.getAccessToken();
    const gqlEndpoint = graphql.defaults({
      baseUrl: 'https://gitlab.com/api',
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
    const viewer = await gqlEndpoint(query);
    return viewer;
  }, []);

  // console.log("value", value);
  // <EntityComponent value={value} />
  if (loading) return <Progress />;
  if (error) return <Alert severity="error">{error.message}</Alert>;
  if (value && value.projects) return (
    <div>
      <DenseTable viewer={value} />
      {/* <EntityComponent value={value} /> */}
    </div>
  );
  
  return (
    <div>
      <Table
        title="List Of User's Repositories"
        options={{ search: false, paging: false }}
        columns={[]}
        data={[]}
      />
      {/* <EntityComponent value={value} /> */}
    </div>

  );
};


