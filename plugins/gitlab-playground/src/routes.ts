import { createRouteRef } from '@backstage/core';


export const rootRouteRef = createRouteRef({
  title: 'gitlab-playground',
});

export const entityRouteRef = createRouteRef({
  title: 'repository',
  params: ['repoName']
})