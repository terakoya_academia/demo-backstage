import { createPlugin, createRoutableExtension } from '@backstage/core';

import { rootRouteRef, entityRouteRef } from './routes';

export const gitlabPlaygroundPlugin = createPlugin({
  id: 'gitlab-playground',
  routes: {
    root: rootRouteRef,
  },
});

export const gitlabPlaygroundPluginEntity = createPlugin({
  id: 'repository',
  routes: {
    root: entityRouteRef,
  }
})

export const GitlabPlaygroundPage = gitlabPlaygroundPlugin.provide(
  createRoutableExtension({
    component: () =>
      import('./components/ExampleComponent').then(m => m.ExampleComponent),
    mountPoint: rootRouteRef,
  }),
);

export const GitlabPlaygroundEntityPage = gitlabPlaygroundPluginEntity.provide(
  createRoutableExtension({
    component: () =>
      import('./components/EntityComponent').then(m => m.EntityComponent),
    mountPoint: entityRouteRef,
  }),
);
